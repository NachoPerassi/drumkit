// recibe un evento keydown y reproduce el sonido asociado a esa tecla
function playSound(e) {
	const audio = document.querySelector(`audio[data-key="${ e.keyCode }"]`);
	const key = document.querySelector(`.key[data-key="${ e.keyCode }"]`);
	audio.currentTime = 0;
	audio.play();
	key.classList.add('playing');
};

// recibe un evento transitionend y remueve la clase que habia sido aplicada
function removeTransition(e){
	if (e.propertyName !== 'transform') return;
	this.classList.remove('playing');
}

const keys = document.querySelectorAll('.key');

keys.forEach(key => key.addEventListener('transitionend', removeTransition));

window.addEventListener('keydown', playSound);